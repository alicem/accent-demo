[GtkTemplate (ui = "/org/gnome/Example/window.ui")]
public class AccentDemo.Window : Adw.ApplicationWindow {
    public Window (Gtk.Application app) {
        Object (application: app);
    }

    static construct {
        typeof (Test).ensure ();
    }
}
