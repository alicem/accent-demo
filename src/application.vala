public class AccentDemo.Application : Adw.Application {
    public Application () {
        Object (
            application_id: "org.gnome.Example",
            flags: ApplicationFlags.DEFAULT_FLAGS
        );
    }

    construct {
        style_manager.color_scheme = FORCE_LIGHT;
    }

    public override void activate () {
        base.activate ();

        var win = active_window ?? new AccentDemo.Window (this);

        win.present ();
    }
}
