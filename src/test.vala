[GtkTemplate (ui = "/org/gnome/Example/test.ui")]
public class AccentDemo.Test : Adw.Bin {
    public bool colored { get; set; default = true; }

    [GtkChild]
    private unowned Gtk.Button button;
    [GtkChild]
    private unowned Gtk.Label label;
    [GtkChild]
    private unowned Gtk.Entry entry;

    construct {
        notify["colored"].connect (update_colors);
        update_colors ();
    }

    private void update_colors () {
        if (colored) {
            button.add_css_class ("suggested-action");
            label.add_css_class ("accent");
            entry.add_css_class ("accent");
        } else {
            button.remove_css_class ("suggested-action");
            label.remove_css_class ("accent");
            entry.remove_css_class ("accent");
        }
    }
}
